package fr.robot;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.robot.dao.QuestionDAO;
import fr.robot.dao.RolesDAO;
import fr.robot.dao.UtilisateurDAO;
import fr.robot.entities.Question;
import fr.robot.entities.Role;
import fr.robot.entities.Utilisateur;
import fr.robot.service.Constant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ChatJeuxApplication.class)
@WebAppConfiguration
public class ChatJeuxApplicationTests {

	@Autowired
	UtilisateurDAO userDao;

	@Autowired
	RolesDAO roleDao;

	@Autowired
	QuestionDAO qstDao;

	// @Test
	// public void testUtilisateur() {
	//
	// Utilisateur user = new Utilisateur();
	// user.setPseudo("joueur1");
	// user.setEmail("joueur@gmail.com");
	// user.setMotDePasse("123456");
	// user.setDateInscription(new Date());
	// userDao.save(user);
	// Role role = new Role();
	// role.setNomRole(Constant.ROLE_JOUEUR);
	// role.setUser(user);
	// roleDao.save(role);
	//
	// }
	//
//	@Test
//	public void testQuestion() {
//
//		Question qst = new Question("label1", "Est ce que", "Auto", 3, 1, new Date());
//		qstDao.save(qst);
//
//	}

	
	@Test
	public void testQuestion() {

//		Question qst = new Question("label1", "Est ce que", "Auto", 3, 1, new Date());
//		qstDao.save(qst);

	}
}
