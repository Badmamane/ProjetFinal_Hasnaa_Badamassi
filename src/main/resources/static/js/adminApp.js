

var app=angular.module('adminApp',[]);

app.controller('adminCtrl',function($scope,$http){
   $scope.getQuestions=function(){
       $http.get('/listQuestion')
           .then(function(response){
              $scope.questions=response.data;
           });
   };
    $scope.getQuestions();
    $scope.question={};
    $scope.addQuestion=function(){
        $scope.question={};
    };
    $scope.editQuestion=function(id){
        $scope.question=$scope.questions[id];
        console.log(id);
        console.log($scope.questions[id]);
    };
    $scope.deleteQuestion=function(id){
        swal({
                title: "Vous êtes sur?",
                text: "Vous êtes sur le point de supprimer cette question définitivement!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui, Je veux la supprimer!",
                closeOnConfirm: false
            },
            function(){
                $http(
                    {
                        method:"POST",
                        url:'/admin/deleteQst',
                        params:{ id : id}
                    }
                ).then(function(response){
                	swal("Supprimé!", "La question a été bien supprimé", "success");
                    })
                        

            });
    };
});
