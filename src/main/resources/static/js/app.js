

var app=angular.module('myApp',[]);
app.controller('appCtrl',function($scope,$http,$filter,$interval,$timeout){
    $scope.getConnectedList=function(){
        $http.get('/joueursEnLigne')
            .then(function(response){
               $scope.connected=response.data;
               $scope.getQuestions();
            });
        $http.get('/listJoueurs')
        .then(function(response){
           $scope.joueurs=response.data;
           $scope.getQuestions();
        });
    };
    $scope.getQuestions=function(){
        $http.get('/listQuestion')
            .then(function(response){
               $scope.questions=response.data;
               $scope.getTopStats();
            });
    };
    $scope.getTimeAndFirstQuestion=function(){
    	$http.get('/getMeTime')
    	.then(function(response){
    		$scope.timeLeft=response.data;
    		$timeout(function(){
    			$http.get('/getMeAQuestion')   
    	    	.then(function(response){
    				$scope.currentQuestion=response.data;
    	    	});
    		},parseInt($scope.timeLeft+''));
    	});
    }
    $scope.getUserInfo=function(){
        $http.get('/getMeUserInfo')
            .then(function(response){
               $scope.user=response.data;
               $scope.user.dateInscription=(new Date($scope.user.dateInscription)).toUTCString();
               $scope.user.dateDerniereConnexion=(new Date($scope.user.dateDerniereConnexion)).toUTCString();
            });
        
        $http.get('/getMeAQuestion') 
    	.then(function(response){
			$scope.currentQuestion=response.data;
			$scope.getTimeAndFirstQuestion();
    	});
        $interval(function(){
        	$http.get('/getMeAQuestion')
        	.then(function(response){
    			$scope.currentQuestion=response.data;
    	});
        	$scope.getConnectedList();
        },10000); // 3 minutes
    }
    $scope.getTopStats=function(){
    	$scope.topPlayer=$filter('orderBy')($scope.joueurs, 'nbrPoints',true)[0];
    	$scope.topQuestion=$filter('orderBy')($scope.questions, 'nbrFoisPosee',true)[0];
    };
    $scope.getConnectedList();
    $scope.getUserInfo();
    $scope.isConnected=function(state){
        if(state){
            return "connecte";
        }
        else{
            return "deconnecte";
        }
    };
    $scope.isAsking=true;
    $scope.submitChat=function(){
        if($scope.chat=='!points'){ //les points du joueur
            $scope.closeClassement();
            $scope.getUserInfo();
            $scope.isMsg=true;
            $scope.msg="Vous avez "+$scope.user.nbrPoints+" points."

        }
        else if($scope.chat=='!classement'){ //le classement courant
            $scope.isAsking=false;
            $scope.isClassement=true;
        }
        
        else { // reponse a vérifié

            $http(
                {
                    method:"POST",
                    url:'/verifierReponse?reponse='+$scope.chat
                }
            )
                .then(function(response){
                   $scope.result=response.data;
                   $scope.isMsg=true;
                   console.log($scope.result);
                   if($scope.result=="false"){
                	   $scope.msg="votre réponse est fausse";
                   }
                   else{
                	   $scope.msg=$scope.result;
                   }
                   $scope.getConnectedList();
                   console.log($scope.topPlayer);
                });
            
        }
    };
    $scope.closeClassement=function(){
        $scope.isClassement=false;
        $scope.isAsking=true;
    }
});
