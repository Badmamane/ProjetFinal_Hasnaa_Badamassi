

var app=angular.module('statsApp',[]);

app.controller('statsCtrl',function($scope,$http){
	$scope.getConnectedList=function(){
        $http.get('/listJoueurs')
            .then(function(response){
               $scope.joueurs=response.data;
               $scope.getQuestions();
            });
        $http.get('/joueursEnLigne')
        .then(function(response){
           $scope.connected=response.data;
           $scope.getQuestions();
        });
    };
    $scope.getQuestions=function(){
        $http.get('/listQuestion')
            .then(function(response){
               $scope.questions=response.data;
               $scope.stats={
               		questions:$scope.questions,
               		joueurs:$scope.joueurs
               }
            });
    };
    $scope.getUserInfo=function(){
        $http.get('/getMeUserInfo?id=1')
            .then(function(response){
               $scope.user=response.data;
               $scope.user.dateInscription=(new Date($scope.user.dateInscription)).toUTCString();
               $scope.user.dateDerniereConnexion=(new Date($scope.user.dateDerniereConnexion)).toUTCString();
            });
    }
    $scope.getAllStats=function(){
        $scope.getConnectedList();
    };
   $scope.refreshStats=function(){
       $scope.getAllStats();
       $scope.getUserInfo();
   };
   $scope.refreshStats();
});
