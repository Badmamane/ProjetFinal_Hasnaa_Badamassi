package fr.robot.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.robot.entities.Utilisateur;

public interface UtilisateurDAO extends JpaRepository<Utilisateur, Integer> {

	public Utilisateur findByEmail(String email);
}
