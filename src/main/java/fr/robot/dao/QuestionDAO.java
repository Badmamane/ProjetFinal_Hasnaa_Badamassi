package fr.robot.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.robot.entities.Question;

public interface QuestionDAO extends JpaRepository<Question, Integer>{

}
