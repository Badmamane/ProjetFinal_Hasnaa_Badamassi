package fr.robot.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.robot.entities.Role;

public interface RolesDAO extends JpaRepository<Role, Long>{

}
