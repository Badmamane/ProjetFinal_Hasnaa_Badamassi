package fr.robot.config;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import fr.robot.dao.UtilisateurDAO;
import fr.robot.entities.Utilisateur;
import fr.robot.service.Constant;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    SessionRegistry sessionRegistry;
    @Autowired
    UtilisateurDAO userDao;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // super.configure(http);
        http.csrf().disable();
        // http.rememberMe()
        // .key("remember")
        // .rememberMeParameter("remember")
        // .tokenValiditySeconds(3600);

        http.authorizeRequests()
                .antMatchers("/css/**", "/img/**", "/js/**", "/fonts/**", "/login", "/register", "/registerSuccess")
                .permitAll().antMatchers("/admin").hasAuthority("ROLE_ADMIN").anyRequest().authenticated()

                .and().rememberMe().key("remember").rememberMeParameter("remember").tokenValiditySeconds(3600).and()
                .exceptionHandling().accessDeniedPage("/403").and()

                .formLogin().loginPage("/login").passwordParameter("password").usernameParameter("email")
                .failureUrl("/login?error").defaultSuccessUrl("/").successHandler(authenticationSuccessHandler)

                // .failureHandler(new AuthenticationFailureHandlerImpl())
                .permitAll().and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessHandler(logoutSuccessHandler)
                .invalidateHttpSession(true).logoutSuccessUrl("/login").and().sessionManagement().maximumSessions(1) // How
                // many
                // session
                // the
                // same
                // user
                // can
                // have?
                // This
                // can
                // be
                // any
                // number
                // you
                // pick
                .expiredUrl("/login?expired").sessionRegistry(sessionRegistry);

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // auth
        // .inMemoryAuthentication()
        // .withUser("user").password("password").roles("USER")
        // .and()
        // .withUser("user1").password("password1").roles("TEST")
        // .and()
        // .withUser("user2").password("password2").roles("TEST").disabled(true);

        auth.userDetailsService(userDetailsService);
    }

    @Bean(name = "sessionRegistry")
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new ProcessAuthSuccess();
    }

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    protected static class ProcessAuthSuccess extends SimpleUrlAuthenticationSuccessHandler {
        @Autowired
        UtilisateurDAO userDao;

        @Override
        public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
                                            final Authentication authentication) throws IOException, ServletException {

            super.onAuthenticationSuccess(request, response, authentication);
            Utilisateur user = userDao.findByEmail(authentication.getName());
            user.setDateDerniereConnexion(new Date());
            userDao.save(user);
            Constant.listJoueursEnLigne.add(user);
        }
    }



    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new ProcessLogoutSuccess();
    }

    @Autowired
    private LogoutSuccessHandler logoutSuccessHandler;


    protected static class ProcessLogoutSuccess extends SimpleUrlLogoutSuccessHandler {
        @Autowired
        UtilisateurDAO userDao;

        @Override
        public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
                                    Authentication authentication) throws IOException, ServletException {
            super.onLogoutSuccess(request, response, authentication);
            String email = authentication.getName();
            System.out.println("email  "+email);
            for (Iterator iterator = Constant.listJoueursEnLigne.iterator(); iterator.hasNext();) {
                Utilisateur user = (Utilisateur) iterator.next();
                if(user.getEmail().equals(email))
                {
                    System.out.println(Constant.listJoueursEnLigne.remove(user));
                    break;
                }
            }

        }




    }
}