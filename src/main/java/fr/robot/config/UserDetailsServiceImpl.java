package fr.robot.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import fr.robot.dao.UtilisateurDAO;
import fr.robot.entities.Utilisateur;

@Component
public class UserDetailsServiceImpl extends Utilisateur implements UserDetailsService {

	@Autowired
	private UtilisateurDAO userService;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Utilisateur userDB = userService.findByEmail(email);

		if (userDB == null) {
			throw new UsernameNotFoundException("Email " + email + " not found");
		}

		org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User(
				userDB.getEmail(), userDB.getMotDePasse(), true, true, true, true, buildUserAuthority(userDB.getRoles()));

		return user;

	}

	private List<GrantedAuthority> buildUserAuthority(Collection<fr.robot.entities.Role> collection) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		for (fr.robot.entities.Role userRole : collection) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getNomRole()));
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}

}
