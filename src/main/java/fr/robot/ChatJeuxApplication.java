package fr.robot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatJeuxApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatJeuxApplication.class, args);
	}
}
