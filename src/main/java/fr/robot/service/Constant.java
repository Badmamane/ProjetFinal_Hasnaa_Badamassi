package fr.robot.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.robot.entities.Utilisateur;

public interface Constant {

	String ROLE_JOUEUR = "ROLE_JOUEUR";

	Set<Utilisateur> listJoueursEnLigne = new HashSet<Utilisateur>();

	List<QuestionCourante> listQuestion = new ArrayList<QuestionCourante>();

	List<String> listReponseCorrect = new ArrayList<String>();
	long NBRE_SECONDES = 30000;

}
