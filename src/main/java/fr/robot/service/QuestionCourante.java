package fr.robot.service;

import java.util.Date;

import fr.robot.entities.Question;

public class QuestionCourante {

	Question qst;
	Date startDate;

	public QuestionCourante(Question qst, Date startDate) {
		super();
		this.qst = qst;
		this.startDate = startDate;
	}

	public Question getQst() {
		return qst;
	}

	public void setQst(Question qst) {
		this.qst = qst;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}
