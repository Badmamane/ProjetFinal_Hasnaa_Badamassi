package fr.robot.controller;

import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.robot.dao.QuestionDAO;
import fr.robot.entities.Question;

@Controller
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
@RequestMapping(value = "/admin")
public class QuestionAdminController {

	@Autowired
	QuestionDAO qstDao;

	@RequestMapping(value = { "/", "" }, method = RequestMethod.GET)
	public String admin(Model model) {

		model.addAttribute("question", new Question());
		return "admin";
	}

	@RequestMapping(value = "/ajoutQst", method = RequestMethod.POST)
	public String ajoutQuestion(Model model, @ModelAttribute("question") Question qst) {

		qst.setDateAjout(new Date());
		qstDao.save(qst);
		return "redirect:/admin";
	}

	@RequestMapping(value = "/editQst", method = RequestMethod.POST)
	public String editQuestion(Model model, @ModelAttribute("question") Question qst) {

		Question qstEdit = qstDao.getOne(qst.getId());
		qstEdit.setLabel(qst.getLabel());
		qstEdit.setPointsMaximales(qst.getPointsMaximales());
		qstEdit.setQuestionText(qst.getQuestionText());
		qstEdit.setReponse(qst.getReponse());
		qstDao.save(qstEdit);
		return "redirect:/admin";
	}

	@RequestMapping(value = "/deleteQst", method = RequestMethod.POST)
	public String deleteQuestion(Model model, @RequestParam("id") int id) {

		qstDao.delete(id);
		return "redirect:/admin";
	}

}
