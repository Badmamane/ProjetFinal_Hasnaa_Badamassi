package fr.robot.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.robot.dao.RolesDAO;
import fr.robot.dao.UtilisateurDAO;
import fr.robot.entities.Role;
import fr.robot.entities.Utilisateur;
import fr.robot.service.Constant;

@Controller
public class LoginAndRegister {

	@Autowired
	UtilisateurDAO userDao;

	@Autowired
	RolesDAO roleDao;

	@RequestMapping(value = "/login")
	public String login(HttpServletRequest req, Model model,
			@RequestParam(value = "error", required = false) String error) {
		AuthenticationException exception = (AuthenticationException) req.getSession()
				.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
		if (exception != null) {
			model.addAttribute("error", true);
			model.addAttribute("exception", exception.getMessage());
		}
		model.addAttribute("newUser", new Utilisateur());
		return "loginForm";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(Utilisateur user) {

		user.setDateInscription(new Date());
		userDao.save(user);

		// Assigner ROLE_JOUEUR au nouveau utilisateur
		Role role = new Role(Constant.ROLE_JOUEUR);
		role.setUser(user);
		roleDao.save(role);

		return "/registerSuccess";
	}

	@RequestMapping(value = "/register")
	public String registerForm() {

		return "/loginForm";
	}

	@RequestMapping(value = "/registerSuccess")
	public String registerSucess() {

		return "/registerSuccess";
	}

}
