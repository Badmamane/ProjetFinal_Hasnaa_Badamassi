package fr.robot.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.robot.dao.QuestionDAO;
import fr.robot.dao.UtilisateurDAO;
import fr.robot.entities.Question;
import fr.robot.entities.Utilisateur;
import fr.robot.service.Constant;
import fr.robot.service.QuestionCourante;

@org.springframework.web.bind.annotation.RestController
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class RestController {

	@Autowired
	QuestionDAO qstDao;

	@Autowired
	UtilisateurDAO userDao;

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	SessionRegistry sessionRegistry;

	@RequestMapping(value = "/listQuestion")
	public String listeQuestion(ModelAndView model) throws JsonProcessingException {

		List<Question> listQst = qstDao.findAll();
		return mapper.writeValueAsString(listQst);

	}

	@RequestMapping(value = "/listJoueurs")
	public String listPersonnesConnecte(ModelAndView model) throws JsonProcessingException {

		List<Utilisateur> listUsers = userDao.findAll();

		return mapper.writeValueAsString(listUsers);

	}

	@RequestMapping(value = "/getMeAQuestion")
	public String getMeAQuestion(HttpServletRequest req) throws JsonProcessingException {

		if (Constant.listQuestion.isEmpty()) {

			List<Question> qstList = qstDao.findAll();

			Random random = new Random();
			int randIndex = random.nextInt(qstList.size());

			Question qst = qstList.get(randIndex);
			qst.setNbrFoisPosee(qst.getNbrFoisPosee() + 1);
			qstDao.save(qst);
			req.getSession().setAttribute("questionPosee", qst);
			List<String> listReponseCorrect = new ArrayList<String>();
			req.getSession().setAttribute("listReponseCorrect", listReponseCorrect);
			Constant.listReponseCorrect.clear();
			Constant.listQuestion.add(new QuestionCourante(qst, new Date()));
			return mapper.writeValueAsString(qst);

		} else {
			Long diffDate = (new Date()).getTime()
					- Constant.listQuestion.get(Constant.listQuestion.size() - 1).getStartDate().getTime();

			if (diffDate < Constant.NBRE_SECONDES) {
				return mapper.writeValueAsString(Constant.listQuestion.get(Constant.listQuestion.size() - 1).getQst());
			}

			else {
				List<Question> qstList = qstDao.findAll();

				Random random = new Random();
				int randIndex = random.nextInt(qstList.size());
				Question qst = qstList.get(randIndex);
				qst.setNbrFoisPosee(qst.getNbrFoisPosee() + 1);
				qstDao.save(qst);
				req.getSession().setAttribute("questionPosee", qst);
				List<String> listReponseCorrect = new ArrayList<String>();
				req.getSession().setAttribute("listReponseCorrect", listReponseCorrect);
				Constant.listReponseCorrect.clear();

				Constant.listQuestion.add(new QuestionCourante(qst, new Date()));
				return mapper.writeValueAsString(qst);
			}
		}

	}

	/*
	 * pour retourner les infos d'un utilisateur connecté
	 */
	@RequestMapping(value = "/getMeUserInfo")
	public String getMeUserInfo() throws JsonProcessingException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailUser = (String) auth.getName();
		return mapper.writeValueAsString(userDao.findByEmail(emailUser));
	}


	@RequestMapping(value = "/getTopStats")
	public String getTopStats() {

		return null;
	}

	/*
	 * cette méthode retourne toutes les stats (à savoir les joueurs et les
	 * question les plus posée et la liste des connectés
	 */
	@RequestMapping(value = "/getAllStats")
	public String getAllStats() {
		// # getClassement te donne la liste des utilisateurs
		// # listQuestions te donne la listes des questions, fais un trie et
		// continue le travail
		// #listepersonnesconnecté te donne la liste
		return null;
	}

	/*
	 * cette méthode retourne le classement des joueurs (les 5 ou 10 premiers)
	 */
	@RequestMapping(value = "/getClassement")
	public String getClassement() throws JsonProcessingException {
		return mapper.writeValueAsString(userDao.findAll());
	}

	/*
	 * cette méthode verifie si une réponse envoyé est juste ou pas il retourne
	 * 1 si c'est la premiere reponse, 2 ... ainsi que le nombre de points
	 * gagnés
	 */
	@RequestMapping(value = "/verifierReponse")
	public String verifierReponse(@RequestParam("reponse") String reponse, HttpServletRequest req)
			throws JsonProcessingException {

		Question qstPosee = Constant.listQuestion.get(Constant.listQuestion.size()-1).getQst();
		if (qstPosee.getReponse().equals(reponse)) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String emailUser = (String) auth.getName();
			// Check if the user has respond to this question
			
			if (Constant.listReponseCorrect.contains(emailUser)) {
				return mapper.writeValueAsString("Vous avez déjà repondu à cette question ...");
			} else {
				Constant.listReponseCorrect.add(emailUser);
				int classement = Constant.listReponseCorrect.indexOf(emailUser) + 1;
				Utilisateur user = userDao.findByEmail(emailUser);
				user.setNbrQuestions(user.getNbrQuestions() + 1);
				if (classement <= 3) {
					user.setNbrPoints(user.getNbrPoints() + (4-classement)*qstPosee.getPointsMaximales() / 3);
					
					
					userDao.save(user);
					return mapper.writeValueAsString("Réponse correcte ! Vous êtes le " + classement
							+ " eme qui a pu répondu à cette question ! Vous avez gagné "+(4-classement)*qstPosee.getPointsMaximales() / 3+" points");
				} else {
					userDao.save(user);
					return mapper.writeValueAsString(
							"Réponse correcte ! Mais déjà 3 personnes ont pu répondu à cette question !");
				}

			}

		} else {
			return mapper.writeValueAsString("false");
		}

	}

	@RequestMapping(value = "/getMeTime")
	public String getMeTime(HttpServletRequest req) throws JsonProcessingException {
		getMeAQuestion(req);
		return mapper.writeValueAsString((new Date()).getTime()
				- Constant.listQuestion.get(Constant.listQuestion.size() - 1).getStartDate().getTime());

	}

	// Retourne la liste des users en ligne
	@RequestMapping(value = "/joueursEnLigne")
	public String loggedUsers() throws JsonProcessingException {

		return mapper.writeValueAsString(Constant.listJoueursEnLigne);
	}

	

}
