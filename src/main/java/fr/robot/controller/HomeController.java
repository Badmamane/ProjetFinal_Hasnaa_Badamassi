package fr.robot.controller;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@RequestMapping(value = "/")
	public String home(Locale locale, Model model) {

		return "accueil";
	}

	@RequestMapping(value = "/statistique")
	public String statistique(Model model) {
		return "stats";
	}

	@RequestMapping(value = "/profil")
	public String profil(Model model) {
		return "profil";
	}

}
