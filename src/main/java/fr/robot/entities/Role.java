package fr.robot.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Role {
	
	@Id
	@GeneratedValue
	private Long ID;
	
	private String nomRole;
	
	public Role(String nomRole) {
		super();
		this.nomRole = nomRole;
	}
	public Role() {
		super();
	
	}

	@ManyToOne
	@JoinColumn(name="id_User")
	@JsonIgnore
	Utilisateur user;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getNomRole() {
		return nomRole;
	}

	public void setNomRole(String nomRole) {
		this.nomRole = nomRole;
	}

	public Utilisateur getUser() {
		return user;
	}

	public void setUser(Utilisateur user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Role [ID=" + ID + ", nomRole=" + nomRole + "]";
	}
	
	

}
