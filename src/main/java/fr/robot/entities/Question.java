package fr.robot.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Question {

	@Id
	@GeneratedValue
	private int id;
	private String label;
	
	private String questionText;
	private String reponse;
	private int pointsMaximales;
	private int nbrFoisPosee;
	private Date dateAjout;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getReponse() {
		return reponse;
	}
	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
	public int getPointsMaximales() {
		return pointsMaximales;
	}
	public void setPointsMaximales(int pointsMaximales) {
		this.pointsMaximales = pointsMaximales;
	}
	public int getNbrFoisPosee() {
		return nbrFoisPosee;
	}
	public void setNbrFoisPosee(int nbrFoisPosee) {
		this.nbrFoisPosee = nbrFoisPosee;
	}
	public Date getDateAjout() {
		return dateAjout;
	}
	public void setDateAjout(Date dateAjout) {
		this.dateAjout = dateAjout;
	}
	@Override
	public String toString() {
		return "Question [id=" + id + ", label=" + label + ", question=" + questionText + ", reponse=" + reponse
				+ ", pointsMaximales=" + pointsMaximales + ", nbrFoisPosee=" + nbrFoisPosee + ", dateAjout=" + dateAjout
				+ "]";
	}
	public String getQuestionText() {
		return questionText;
	}
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	
	
	

	

}
